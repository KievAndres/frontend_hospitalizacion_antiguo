import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';
  import Widget04 from '../NotaInternacionVista/Widget04';


class ResumenDeAlta extends Component {
    constructor(props){
        super(props)

        this.state = {
            internados: [],
            primary: false,
            Nombre:'',
            Direccion:'',
            Telefono: '',
            Fecha:'',
            Hora:'',
            Descripcion:'',
            Diagnostico:'',
            Indicaciones:'',

            search: '',
            data: [],
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/internados/');
            const internados = await respuesta.json();
            console.log(internados)
            this.setState({
                internados
            });
        } catch(e){
            console.log(e);
        }
    }

    handleChange(value){    
      console.log(value);
      this.setState({ muestraBoton: true });
    }

    handleOnSubmit(e){
      console.log(this.state.Telefono)
      alert(
        'Hola: ' + this.state.Nombre + 
        '\nDireccion: ' + this.state.Direccion + 
        '\nTelefono: ' + this.state.Telefono +
        '\nFecha: ' + this.state.Fecha +
        '\nHora: ' + this.state.Hora +
        '\nDescripcion ' + this.state.Descripcion +
        '\nDiagnostico ' + this.state.Diagnostico +
        '\nIndicaciones ' + this.state.Indicaciones
      );
      e.preventDefault();

      let url =''
      let data = {}

      url = 'http://127.0.0.1:8000/resumens/'
      data = {
          nombre: this.state.Nombre,
          direccion: this.state.Direccion,
          telefono: this.state.Telefono,
          fecha: this.state.Fecha,
          hora: this.state.Hora,
          descripcion: this.state.Descripcion,
          diagnostico: this.state.Diagnostico,
          indicaciones: this.state.Indicaciones
      }

      try{
        fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Succes:', response));
      } catch(e){
        console.log(e)
      }

      this.setState({
        primary: !this.state.primary,
      });

      // e.target.reset();
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    updateSearch(event){
      this.setState({search: event.target.value.substr(0,20)});
  }

   render() {
        let historiales_filtrados = this.state.internados.filter(
          (item) => {
              return item.nombre.toUpperCase().indexOf(
                this.state.search.toUpperCase()) !== -1; 
          }
      );

        return (
            <div className="animated fadeIn">
            <h1>Resumen de Alta</h1>
            <br/><br/>
                <Row>
                

                <Col sm="6" md="2">
                    <Widget04 icon="icon-people" color="info" header={ this.state.internados.length } value="75">Internados</Widget04>
                </Col>  

                <Col md="1">      
                    <h1>
                    Filtro
                    </h1>
                </Col>

                <h1>
                    <Col md="5">
                        <ul>
                            <input type="text"
                            value={ this.state.search }
                            onChange={ this.updateSearch.bind(this) }
                            />
                        </ul>
                    </Col>
                </h1>

                <Col sm="6" md="2">
                    <Widget04 icon="icon-people" color="success" header={ historiales_filtrados.length } value="75" invert>Filtrados</Widget04>
                </Col> 
                    {/* { this.state.muestraBoton ? <Boton />: null } */}

                    <br/>

                <Col xs="12" md="12">
                {historiales_filtrados.map(item => (
                  <div key={item.id}>

                <Card>
                    <CardHeader>
                    <strong><h1>{item.nombre}</h1></strong> <h4>RESUMEN DE ALTA - SSU </h4> 
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Fecha de Ingreso</strong> </Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.fecha}</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Hora de Ingreso</strong> </Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.hora}</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Fecha de Egreso</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="date" 
                            name="Direccion" 
                            onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Servicio</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="text" 
                            name="Direccion" 
                            placeholder="Ej. Cirugía"
                            onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>


                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Diagnóstico de Ingreso</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h6>{item.diagnostico}</h6>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Diagnóstico de Egreso</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion" 
                            placeholder="Diágnostico con el que sale el paciente del SSU"
                            onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Tratamiento</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h6>{item.indicaciones}</h6>
                        </Col>
                        </FormGroup>
                        
                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Antecedents, Anamnesis y Examen Físico</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion" 
                            placeholder="Antecedentes médicos"
                            onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Hallazgos significativos de Laboratorio</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion" 
                            placeholder="Laboratorios realizados"
                            onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Evolución intrahospitalaria</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion" 
                            placeholder="Mejorías observadas desde la última intervención quirúrgica"
                            onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Condiciones de alta, tratamiento y control post hospitalario</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <Input 
                            type="textarea" 
                            name="Direccion"
                            placeholder="Condición del paciente" 
                            onChange={e => this.setState({ Direccion: e.target.value })}
                          />
                        </Col>
                        </FormGroup>
                    </Form>
                    </CardBody>
                    <CardFooter>
                    <Button onClick={this.handleOnSubmit} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button>
                    </CardFooter>
                </Card>
                </div>    
                ))}
                </Col>
            </Row>
        </div>
        );
    }
}
export default ResumenDeAlta;