import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';

class NotaInternacion extends Component {
    constructor(props){
        super(props)

        this.state = {
            internados: [],
            primary: false,
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/internados/');
            const internados = await respuesta.json();
            this.setState({
                internados
            });
        } catch(e){
            console.log(e);
        }
    }

    handleChange(e){
        let {value, name, id} = e.target
        console.log(value, name, id)

        let internados_copia = this.state.internados

        // if(name === "")
        this.setState({
            internados : internados_copia
        });
    }

    handleOnSubmit(e){
        e.preventDefault()

        console.log(this.state.internados);
        let url= ''
        let data = {}

        this.state.internados.map((item, indice)=>{

            url = 'http://127.0.0.1:8000/internados/'
            data = {
                
            }

            try{
                fetch(url, {
                    method: 'POST',
                    body: JSON.stringify(data),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                }).then(res => res.json())
                .catch(error => console.error('Error': error))
                .then(response => console.log('Success:', response));
            }catch(e){
                console.log(e);
            }
        });

        
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                <Col xs="12" md="12">
                <Card>
                    <CardHeader>
                    <strong>Nota de Internacion</strong> PROMES
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                        <FormGroup row>
                        <Col md="3">
                            <Label>Static</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <p className="form-control-static">Username</p>
                        </Col>
                        </FormGroup>
                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Nombre</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input type="text" id="text-input" name="text-input" placeholder="Ej. Andres Mendoza Rojas" />
                            <FormText color="muted">Coloca tu nombre completo</FormText>
                        </Col>
                        </FormGroup> 

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Dirección</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input type="text" id="text-input" name="text-input" placeholder="Ej. Zona Villa Copacabana" />
                            <FormText color="muted">Coloca el lugar de tu residencia actual</FormText>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Número telefónico</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input type="number" id="number-input" name="number-input" placeholder="Ej. 700 00 000" />
                            <FormText color="muted">Coloca el lugar de tu residencia actual</FormText>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="date-input">Fecha de Atención</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input type="date" id="date-input" name="date-input" placeholder="date" />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="date-input">Hora de Atención</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input type="time" />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Descripcion</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                                placeholder="Decripción del médico" />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Diagnóstico</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                                placeholder="Diagnóstico del médico" />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Tratamiento</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                                placeholder="Tratamiento del médico" />
                        </Col>
                        </FormGroup>
                        
                        
                    </Form>
                    </CardBody>
                    <CardFooter>
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button>
                    </CardFooter>
                </Card>
                </Col>
            </Row>
        </div>
        );
    }
}
export default NotaInternacion;