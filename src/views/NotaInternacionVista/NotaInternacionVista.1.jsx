import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';
import Search from 'react-search-box';
import Boton from "./Boton.js";

class NotaInternacionVista extends Component {
    constructor(props){
        super(props)

        this.state = {
            internados: [],
            primary: false,
            Nombre:'',
            Direccion:'',
            Telefono: '',
            Fecha:'',
            Hora:'',
            Descripcion:'',
            Diagnostico:'',
            Indicaciones:'',

            search: '',
            data: [], //Esto es para el buscador
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/internados/');
            const internados = await respuesta.json();
            console.log(internados)
            this.setState({
                internados
            });
        } catch(e){
            console.log(e);
        }
    }

    handleChange(value){    
      console.log(value);
      this.setState({ muestraBoton: true });
    }

    handleOnSubmit(e){
      console.log(this.state.Telefono)
      alert(
        'Hola: ' + this.state.Nombre + 
        '\nDireccion: ' + this.state.Direccion + 
        '\nTelefono: ' + this.state.Telefono +
        '\nFecha: ' + this.state.Fecha +
        '\nHora: ' + this.state.Hora +
        '\nDescripcion ' + this.state.Descripcion +
        '\nDiagnostico ' + this.state.Diagnostico +
        '\nIndicaciones ' + this.state.Indicaciones
      );
      e.preventDefault();

      let url =''
      let data = {}

      url = 'http://127.0.0.1:8000/internados/'
      data = {
          nombre: this.state.Nombre,
          direccion: this.state.Direccion,
          telefono: this.state.Telefono,
          fecha: this.state.Fecha,
          hora: this.state.Hora,
          descripcion: this.state.Descripcion,
          diagnostico: this.state.Diagnostico,
          indicaciones: this.state.Indicaciones
      }

      try{
        fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Succes:', response));
      } catch(e){
        console.log(e)
      }

      this.setState({
        primary: !this.state.primary,
      });
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    updateSearch(event){
        this.setState({search: event.target.value.substr(0,20)});
    }

    render() {
        let historiales_filtrados = this.state.internados.filter(
            (item) => {
                return item.nombre.toUpperCase().indexOf(
                  this.state.search.toUpperCase()) !== -1; 
            }
        );
        return (
            <div className="animated fadeIn">
                <Row>
                <Col xs="12" md="12">
                <div class="buscador">
                  <h1>Buscador</h1>
                  <ul>
                    <input type="text"
                      value={ this.state.search }
                      onChange={ this.updateSearch.bind(this) }
                    />
                  </ul>
                </div>
                { this.state.muestraBoton ? <Boton />: null }

                <br/>

                {historiales_filtrados.map(item => (
                  <div key={item.id}>

                <Card>
                    <CardHeader>
                    <strong><h1>{item.nombre}</h1></strong> <h4>Nota Internacion - PROMES </h4> 
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Dirección</strong> </Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.direccion}</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input"><strong>Número telefónico</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.telefono}</h5>
                        </Col>
                        </FormGroup>


                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Descripcion</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h6>{item.descripcion}</h6>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Diagnóstico</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.diagnostico}</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input"><strong>Indicaciones</strong></Label>
                        </Col>
                        <Col xs="12" md="9">
                          <h5>{item.indicaciones}</h5>
                        </Col>
                        </FormGroup>
                        
                        
                    </Form>


                    </CardBody>
                    <CardFooter>
                    <h4>Fecha de Atención: {item.fecha}</h4>
                    <h5>Hora de Atención: {item.hora}</h5>
                    </CardFooter>
                </Card>
                </div>    
                ))}
                </Col>
            </Row>
        </div>
        );
    }
}
export default NotaInternacionVista;