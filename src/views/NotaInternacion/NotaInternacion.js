import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
  } from 'reactstrap';
  import ColaInternacion from '../ColaInternacion/ColaInternacion';

class NotaInternacion extends Component {
    constructor(props){
        super(props)

        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        this.state = {
            internados: [],
            primary: false,
            Nombre:'',
            Carrera:'',
            Facultad:'',
            Direccion:'',
            Telefono: 67000012,
            Fecha: date,
            Hora:'',
            Descripcion:'',
            Diagnostico:'',
            Indicaciones:'',
            
            date: date
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        // this.handleChange = this.handleChange.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    // async componenyDidMount(){
    //     console.log(this.props.id)
    // }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/afiliados/'+this.props.id+'/');
            const internados = await respuesta.json();
            console.log(internados)
            this.setState({
                internados
            });
        } catch(e){
            console.log(e);
        }
    }

    handleChange( {target}){    
        this.setState({
            [target.name]: target.value,
        })
    }

    handleOnSubmit(e){
      console.log(this.state.Telefono)
      alert(
        'Hola: ' + this.state.internados.nombre1 +' '+this.state.internados.a_paterno+' '+this.state.internados.a_materno + 
        '\nCarrera: ' + this.state.internados.carrera + 
        '\nFacultad: ' + this.state.internados.facultad +
        '\nTelefono: ' + this.state.internados.telefono +
        '\nFecha: ' + this.state.Fecha +
        '\nDescripcion ' + this.state.Descripcion +
        '\nDiagnostico ' + this.state.Diagnostico +
        '\nIndicaciones ' + this.state.Indicaciones
      );
      e.preventDefault();

      let url =''
      let data = {}

      url = 'http://127.0.0.1:8000/internados/'
      data = {
          nombre: this.state.internados.nombre1 +' '+this.state.internados.a_paterno+' '+this.state.internados.a_materno,
          carrera: this.state.internados.carrera,
          facultad: this.state.internados.facultad,
          telefono: this.state.Telefono,
          fecha: this.state.Fecha,
          descripcion: this.state.Descripcion,
          diagnostico: this.state.Diagnostico,
          indicaciones: this.state.Indicaciones
      }
      console.log('-->'+data);
      try{
        fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Succes:', response));
      } catch(e){
        console.log(e)
      }

      this.setState({
        primary: !this.state.primary,
      });


    }

    prueba1 = (e) => {
        e.preventDefault();
        console.log(this.props.id);
        console.log(this.state.date);
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    render() {        
        return (
            <div className="animated fadeIn">
                <Row>
                <Col xs="12" md="12">
                <Card>
                    <CardHeader>
                    <strong><h1>Nota de Internacion</h1></strong><h4> PROMES</h4>
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                        
                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Nombre</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <h5>{this.state.internados.a_paterno} {this.state.internados.a_materno} {this.state.internados.nombre1} {this.state.internados.nombre2} {this.state.internados.nombre3}</h5>
                        </Col>
                        </FormGroup> 

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Carrera</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <h5>{this.state.internados.carrera}</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Facultad</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <h5>{this.state.internados.facultad}</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="text-input">Número telefónico</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <h5>670 000 12</h5>
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="date-input">Fecha de Atención</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <h5>{ this.state.date }</h5>
                        </Col>
                        </FormGroup>

                        {/* <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="date-input">Hora de Atención</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="time" 
                                name="Hora"
                                // placeholder="time"
                                onChange={e => this.setState({ Hora: e.target.value })}
                            />
                        </Col>
                        </FormGroup> */}

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Descripcion</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="text" 
                                name="Descripcion" 
                                // rows="9"
                                placeholder="Decripción del médico" 
                                onChange={e => this.setState({ Descripcion: e.target.value })}
                            />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Diagnóstico</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="textarea" 
                                name="Diagnostico" 
                                placeholder="Diagnóstico del médico"
                                onChange={e => this.setState({ Diagnostico: e.target.value })}
                            />
                        </Col>
                        </FormGroup>

                        <FormGroup row>
                        <Col md="3">
                            <Label htmlFor="textarea-input">Indicaciones</Label>
                        </Col>
                        <Col xs="12" md="9">
                            <Input 
                                type="textarea" 
                                name="Indicaciones" 
                                placeholder="Indicaciones del médico" 
                                onChange={e => this.setState({ Indicaciones: e.target.value })}
                            />
                        </Col>
                        </FormGroup>
                        
                        
                    </Form>
                    </CardBody>
                    <CardFooter>
                    <Button onClick={this.handleOnSubmit} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button onClick={this.prueba1} type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button>
                    </CardFooter>
                </Card>
                </Col>
            </Row>
        </div>
        );
    }
}
export default NotaInternacion;