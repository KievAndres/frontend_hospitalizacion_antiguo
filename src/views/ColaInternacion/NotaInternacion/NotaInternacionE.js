import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
  } from 'reactstrap';
  import ColaInternacion from '../ColaInternacion';

class NotaInternacionE extends Component {
    constructor(props){
        super(props)

        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        this.state = {
            internados: [],
            anamnesis: [],
            primary: false,
            ci:'',
            telefono:'',
            padecimiento:'',
            diagnostico:'',
            indicaciones: '',
            fecha: date
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        // this.handleChange = this.handleChange.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    // async componenyDidMount(){
    //     console.log(this.props.id)
    // }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/afiliados/');
            const internados = await respuesta.json();
            console.log(internados);
            console.log(this.props.match.params.ci);
            internados.map((item)=>{
                console.log(item);
                console.log(item.ci);
                if(this.props.match.params.ci == item.ci){
                    
                    fetch('http://127.0.0.1:8000/afiliados/'+item.id+'/')
                    .then((Response) => Response.json())
                    .then((findresponse) => {
                        this.setState({
                            internados: findresponse
                        })
                    });
                }
                return 0;
            });
        } catch(e){
            console.log(e);
        }
        
        console.log('**********');
        console.log(this.state.internados);
        console.log('...................');

        try{
            const respuesta = await fetch('http://127.0.0.1:8000/anamnesis/');
            const anamnesis = await respuesta.json();
            console.log(anamnesis)
            anamnesis.map((item)=>{
                console.log(item);
                console.log(item.ci);
                if(this.props.match.params.ci == item.ci){
                    
                    fetch('http://127.0.0.1:8000/anamnesis/'+item.id+'/')
                    .then((Response) => Response.json())
                    .then((findresponse) => {
                        this.setState({
                            anamnesis: findresponse
                        })
                    });
                }
                return 0;
            });
        } catch(e){
            console.log(e);
        }
    }

    handleChange( {target}){    
        this.setState({
            [target.name]: target.value,
        })
    }

    handleOnSubmit(e){
      console.log(this.state.Telefono)
      alert(
        'CI: ' + this.state.internados.ci +
        '\nFecha: ' + this.state.fecha +
        '\nTelefono: ' + this.state.telefono + 
        '\nPadecimiento: ' + this.state.padecimiento +
        '\nDiagnostico: ' + this.state.diagnostico +
        '\nIndicaciones: ' + this.state.indicaciones
      );
      e.preventDefault();

      let url =''
      let data = {}

      url = 'http://127.0.0.1:8000/internados/'
      data = {
          ci: this.state.internados.ci,
          fecha: this.state.fecha,
          telefono: this.state.telefono,
          padecimiento_actual: this.state.padecimiento,
          diagnostico: this.state.diagnostico,
          indicaciones: this.state.indicaciones
      }
      console.log('-->'+data);
      try{
        fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Succes:', response));
      } catch(e){
        console.log(e)
      }

      this.setState({
        primary: !this.state.primary,
      });


    }

    prueba1 = (e) => {
        e.preventDefault();
        console.log(this.props.id);
        console.log(this.state.date);
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    render() {        
        return (
            <div className="animated fadeIn">
                <Row>
                <Col xs="0" md="2">
                </Col>
                <Col xs="12" md="8">
                <Card>
                    <CardHeader>
                    <center>
                        <h1><b>Nota de Internacion</b></h1>
                        <h4><b>PROMES</b></h4>
                    </center>
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                        <font size="3">
                            <p>
                                <b>Nombre: </b>{this.state.internados.a_paterno} {this.state.internados.a_materno} {this.state.internados.nombre1}<br/>
                                <b>CI: </b>{this.state.internados.ci}<br />
                                <b>Edad: </b>{ this.state.internados.edad } años<br/>
                                <FormGroup row>
                                    <Label htmlFor="text-input" sm={2} size="lg"><b>Telefono:</b></Label>
                                    <Col sm={10}>
                                        <Input type="number" placeholder="Indique su número de teléfono" bsSize="lg" onChange={e => this.setState({ telefono: e.target.value })}/>
                                    </Col>
                                </FormGroup>
                                <b>Matrícula: </b>678451<br/>
                                <b>Fecha: </b>{ this.state.fecha }<br/>
                            </p>

                            <p>
                                <h4><b>Antecedentes de importancia</b></h4>
                                <b>Antecedentes Familiares: </b>{this.state.anamnesis.antecedentes_familiares}<br/>
                                <b>Antecedentes Patológicos: </b>{this.state.anamnesis.antecedentes_clinicos}<br/>
                                <b>Antecedentes Quirúrgicos: </b>{this.state.anamnesis.antecedentes_quirurgicos}<br/>
                                <center><b>Antecedentes no patológicos</b></center>
                                <Table size="sm" responsive striped>
                                    <tbody>
                                        <tr>
                                            <th scope="row">Condiciones de vida</th>
                                            <td>{this.state.anamnesis.condiciones_de_vida}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Alimentación</th>
                                            <td>{this.state.anamnesis.alimentacion}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Habito Tabáquico</th>
                                            <td>{this.state.anamnesis.habito_tabaquico}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Habito Alcohólico</th>
                                            <td>{this.state.anamnesis.habito_alcoholico}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Somnia</th>
                                            <td>{this.state.anamnesis.somnia}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Diuresis</th>
                                            <td>{this.state.anamnesis.diuresis}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Hábito Intestinal</th>
                                            <td>{this.state.anamnesis.habito_intestinal}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Antecedentes Gineco-Obstétricos</th>
                                            <td>{this.state.anamnesis.antecedentes_g}</td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </p>

                            <p>
                                <h4><b>Padecimiento actual</b></h4>
                                {/* <Label htmlFor="text-input" sm={12} size="lg"><b>Padecimiento actual</b></Label> */}
                                <Col sm={12}>
                                    <Input type="textarea" placeholder="Descripción del padecimiento..." bgsize="lg" onChange={e => this.setState({ padecimiento: e.target.value })}/>
                                </Col>
                            </p>

                            <p>
                                <h4><b>Exploración física</b></h4>
                                <Table responsive>
                                    <center>
                                        <thead>
                                            <th>Presion Arterial</th>
                                            <th>Frecuencia Cardiaca</th>
                                            <th>Frecuencia Respiratoria</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{this.state.anamnesis.presion_arterial}</td>
                                                <td>{this.state.anamnesis.frecuencia_cardiaca}</td>
                                                <td>{this.state.anamnesis.frecuencia_respiratoria}</td>
                                                {/* FALTA TEMPERATURA AXILAR */}
                                            </tr>
                                        </tbody>
                                    </center>
                                </Table>
                            </p>

                            <p>
                                <h4><b>Estudios complementarios</b></h4>
                                --
                            </p>

                            <p>
                                <h4><b>Diagnóstico</b></h4>
                                <Col sm={12}>
                                    <Input type="text" placeholder="Diagnóstico del médico..." bgsize="lg" onChange={e => this.setState({ diagnostico: e.target.value })}/>
                                </Col>
                            </p>

                            <p>
                                <h4><b>Indicaciones</b></h4>
                                <Col sm={12}>
                                    <Input type="textarea" placeholder="Indicaciones del médico..." bgsize="lg" onChange={e => this.setState({ indicaciones: e.target.value })}/>
                                </Col>
                            </p>
                        </font>
                    </Form>
                    </CardBody>
                    <CardFooter>
                    <Button onClick={this.handleOnSubmit} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button onClick={this.prueba1} type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button>
                    </CardFooter>
                </Card>
                </Col>
            </Row>
        </div>
        );
    }
}
export default NotaInternacionE;